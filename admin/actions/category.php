<?php 
include_once('../db.php');

//Delete category
if(isset($_GET['id'])){
  $id = $_GET['id'];
  $query = "DELETE FROM categories where id = $id";
  if ($mysqli->query($query) === TRUE) {
    //redirect to category listing
    header("Location: ../view_categories.php?deletemsg=Category delete successfully");
  } else {
    echo "Error: " . $query . "<br>" . $mysqli->error;
  }
  die;
}

$title = $_POST['title'];
$slug = $_POST['slug'];
$status = $_POST['status'];
$desc = $_POST['description'];

  
if($_POST['submit'] == "Submit"){

  $picturename = "";    
  if(isset($_FILES["picture"]["tmp_name"]) && !empty($_FILES["picture"]["tmp_name"])){
    $target_dir = "../../assets/admin/images/categories/";
    $picturename = basename($_FILES["picture"]["name"]);
     $pictureExt=pathinfo($picturename, PATHINFO_EXTENSION);
     $picture_filename=pathinfo($picturename, PATHINFO_FILENAME);
     $picturename=$picture_filename.Date("mjYHis").".".$pictureExt;
    $target_file = $target_dir . $picturename;
    move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file);
  }
  $category_name="SELECT title FROM categories where title = '$title'";
  $result=$mysqli->query($category_name);
  if($result->num_rows > 0){
    header("Location: ../create_category.php?category_exist=This category already exist");
    die;
  }
  else{
    $query = "INSERT INTO categories (`title`, `slug`, `description`, `picture` , `status`)
    VALUES ('$title', '$slug', '$desc','$picturename', '$status')";
    if ($mysqli->query($query) === TRUE) {
      //redirect to category listing
      header("Location: ../view_categories.php?insertmsg=Category inserted succesfylly");
      
    } else {
      echo "Error: " . $query . "<br>" . $mysqli->error;
    }
die;
  }
}

if($_POST['submit'] == "Update"){
  $id = $_POST['category_id'];

  $query = "UPDATE categories set `title` = '$title', `slug` = '$slug' , `description` = '$desc' , `status` = '$status'";

  if(isset($_FILES["picture"]["tmp_name"]) && !empty($_FILES["picture"]["tmp_name"])){
    $target_dir = "../../assets/admin/images/categories/";
    $picturename = basename($_FILES["picture"]["name"]);
     $pictureExt=pathinfo($picturename, PATHINFO_EXTENSION);
     $picture_filename=pathinfo($picturename, PATHINFO_FILENAME);
     $picturename=$picture_filename."_".time().".".$pictureExt;
    $target_file = $target_dir . $picturename;
    move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file);

    $query = $query." ,`picture`='$picturename'";
  }
  
  $query = $query." where id = $id";
  
  if ($mysqli->query($query) === TRUE) {
  //redirect to category listing
    header("Location: ../view_categories.php?updatemsg=Category updated succesfylly");
    die;
  } else {
    echo "Error: " . $query . "<br>" . $mysqli->error;
  }
}


/*
echo "<pre>";
print_r($_POST);
die;
*/
?>