<?php
    include_once("db.php");
    include_once('includes/header.php'); 
    include_once('includes/sidebar.php');

    $id=$_GET['id'];
    $query="SELECT * FROM products WHERE id=$id";
    $result=$mysqli->query($query);
    $row=$result->fetch_assoc();
    
    $query_cat="SELECT * FROM categories";
    $result_cat=$mysqli->query($query_cat);
?>

<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <h2>Update Product</h2>
	</div>

	<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body">
                        <form action="actions/product.php" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="product_id" value="<?php echo $row['id'];?>">
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="title" type="text" class="form-control" value="<?php echo $row['title']?>" data-validation="length" data-validation-length="1-150" style="border-color="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <select name="category_id" class="form-control show-tick">
                                        <option value="">-- select category --</option>
                                        
                                        <?php
                                            if($result->num_rows > 0){
                                                while($rows_cat=$result_cat->fetch_assoc()){
                                        ?>
                                        <option value="<?php echo $rows_cat['id']?>" <?php if($rows_cat['id'] == $row['category_id']) echo "selected" ?>><?php echo $rows_cat['title']?></option>
                                        <?php  }}?>
                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix picture-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" type="message" class="form-control"><?php echo $row['description']?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="regular_price" type="number" class="form-control" value="<?php echo $row['regular_price']?>" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="sale_price" type="number" class="form-control" value="<?php echo $row['sale_price']?>" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-line picture-box">
                                        <label class="form-img">
                                           <div class="icon-and-name">
                                                <i class="far fa-image"></i>
                                                <span>CHOOSE FILE</span>
                                           </div>
                                            <input name="picture" type="file" class="form-control custom-file-button" >
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-line picture-box">
                                        <?php 
                                            $imgSrc="../../ecommerce/assets/images/no-image.jpg";
                                            if(!empty($row['picture'])){
                                                 $imgSrc="../../ecommerce/assets/images/products/".$row['picture'];  
                                            }
                                        ?>
                                        <img src="<?php echo $imgSrc?>" alt="" width="150">
                                    </div>
                                </div>
                            </div>

                        
                            <div class="row clearfix status-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <span>Status</span>
                                            <div class="radio-btn">
                                                <input type="radio" id="active" name="status" value="active" <?php if($row['status']=="active"){echo "checked";}?>>
                                                <label for="active">Active</label>
                                                <input type="radio" id="inactive" name="status" value="inactive" <?php if($row['status']=="inactive"){echo "checked";}?>>
                                                <label for="inactive">Inactive</label><br>
                                                </div>
                                        </div>
                                    </div>
                                </div> 

                            <input name="submit" type="submit" value="Update" id="submitbtn">
                        </form>
                    </div>
                </div>

            </div>
    </div>
</section>
<?php include_once('includes/footer.php'); ?>