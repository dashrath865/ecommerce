<?php
include_once("db.php");
include_once('includes/header.php');
include_once('includes/sidebar.php');
$query = "SELECT id,title FROM categories";
$result = $mysqli->query($query);
?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Create Product</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form action="actions/product.php" enctype="multipart/form-data" method="post">
                            <h2 class="card-inside-title">Basic Fields</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="title" type="text" class="form-control" placeholder="Title" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <select name="category_id" class="form-control show-tick">
                                        <option value="">-- select category --</option>

                                        <?php
                                        if ($result->num_rows > 0) {
                                            while ($rows = $result->fetch_assoc()) {
                                                ?>
                                                <option value="<?php echo $rows['id'] ?>"><?php echo $rows['title'] ?></option>
                                            <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row clearfix picture-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" type="message" class="form-control" placeholder="description"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="regular_price" type="number" class="form-control" placeholder="Regular Price" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="sale_price" type="number" class="form-control" placeholder="Sale Price" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-line picture-box">
                                        <label class="form-img">
                                            <div class="icon-and-name">
                                                <i class="far fa-image"></i>
                                                <span>CHOOSE FILE</span>
                                            </div>
                                            <input name="picture" type="file" class="form-control custom-file-button" >
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix status-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <span>Status</span>
                                            <div class="radio-btn">
                                                <input type="radio" id="active" name="status" value="active" checked>
                                                <label for="active">Active</label>
                                                <input type="radio" id="inactive" name="status" value="inactive">
                                                <label for="inactive">Inactive</label><br>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="input-group input-group-md">
                                        <span class="input-group-addon">
                                            <input type="checkbox" name='on_sale' class="filled-in" id="ig_checkbox">
                                            <label for="ig_checkbox">On sale ?</label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                                <input name="submit" type="submit" value="Submit" id="submitbtn">
                        </form>
                    </div>
                </div>
            </div>
            </section>
<?php include_once('includes/footer.php'); ?>