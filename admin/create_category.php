<?php include_once('includes/header.php'); 
      include_once('includes/sidebar.php');
?>

<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <h2>Create Category</h2>
	</div>

	<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <?php 
                            if(isset($_GET['category_exist'])){
                        ?>
                            <div class="alert alert-danger danger">
                                <?php echo $_GET['category_exist']?>
                            </div>
                        <?php }?>
                            
                        
                        <div class="body">
                
                        <form action="actions/category.php" enctype="multipart/form-data" method="post">
                            <h2 class="card-inside-title">Basic Fields</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="title" type="text" class="form-control" placeholder="Title" data-validation="length" data-validation-length="1-150" style="border-color="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="slug" type="text" class="form-control" placeholder="slug" data-validation="length" data-validation-length="1-150">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix picture-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" type="message" class="form-control" placeholder="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-line picture-box">
                                        <label class="form-img">
                                           <div class="icon-and-name">
                                                <i class="far fa-image"></i>
                                                <span>CHOOSE FILE</span>
                                           </div>
                                            
                                            <input name="picture" type="file" class="form-control custom-file-button" >
                                        </label>
                                    </div>
                                </div>
                            </div>  

                            <div class="row clearfix status-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <span>Status</span>
                                            <div class="radio-btn">
                                                <input type="radio" id="active" name="status" value="active" checked>
                                                <label for="active">Active</label>
                                                <input type="radio" id="inactive" name="status" value="inactive">
                                                <label for="inactive">Inactive</label><br>
                                                </div>
                                        </div>
                                    </div>
                            </div> 

                            <input name="submit" type="submit" value="Submit" id="submitbtn">
                        </form>
                    </div>
                </div>

            </div>
    </div>
</section>
<?php include_once('includes/footer.php'); ?>