<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
	<!-- User Info -->
	<div class="user-info">
	    <div class="image">
		<img src="../assets/images/user.png" width="48" height="48" alt="User" />
	    </div>
	    <div class="info-container">
		<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
		<div class="email">john.doe@example.com</div>
		<div class="btn-group user-helper-dropdown">
		    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
		    <ul class="dropdown-menu pull-right">
			<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
			<li role="separator" class="divider"></li>
			<li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
			<li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
			<li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
			<li role="separator" class="divider"></li>
			<li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
		    </ul>
		</div>
	    </div>
	</div>
	<!-- #User Info -->
	<!-- Menu -->
	<div class="menu">
	    <ul class="list">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active">
		    <a href="index.php">
			<i class="material-icons">home</i>
			<span>Home</span>
		    </a>
		</li>
		
		<li>
		    <a href="javascript:void(0);" class="menu-toggle">
			<i class="material-icons">assignment</i>
			<span>Categories</span>
		    </a>
		    <ul class="ml-menu">
			<li>
			    <a href="create_category.php">Create New</a>
			</li>
			<li>
			    <a href="view_categories.php">View All</a>
			</li>
		    </ul>
		</li>

		<li>
		    <a href="javascript:void(0);" class="menu-toggle">
			<i class="fas fa-box-open"></i>
				<span>Product</span>
		    </a>
		    <ul class="ml-menu">
			<li>
			    <a href="create_product.php">Create New</a>
			</li>
			<li>
			    <a href="view_products.php">View All</a>
			</li>
		    </ul>
		</li>
		
	    </ul>
	</div>
	<!-- #Menu -->
    <!-- #END# Right Sidebar -->
</section>