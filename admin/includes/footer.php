<!-- Jquery Core Js -->
<script src="../assets/admin/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="../assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="../assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="../assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     
<!-- Waves Effect Plugin Js -->
<script src="../assets/admin/plugins/node-waves/waves.js"></script>    

<!-- Jquery CountTo Plugin Js -->
<script src="../assets/admin/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="../assets/admin/plugins/raphael/raphael.min.js"></script>
<script src="../assets/admin/plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="./assets/plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="../assets/admin/plugins/flot-charts/jquery.flot.js"></script>
<script src="../assets/admin/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="../assets/admin/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="../assets/admin/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="../assets/admin/plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="../assets/admin/plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- Custom Js -->
<script src="../assets/admin/js/admin.js"></script>
<script src="../assets/admin/js/pages/index.js"></script>

<!-- Demo Js -->
<script src="../assets/admin/js/demo.js"></script>
<script src="../assets/admin/js/all.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
 $.validate();
</script>
</body>

</html>