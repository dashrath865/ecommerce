<?php
include_once('db.php'); 
include_once('includes/header.php'); 
include_once('includes/sidebar.php'); 
$query="SELECT products.*,categories.title AS category_title FROM products
JOIN categories ON categories.id = products.category_id";

$result=$mysqli->query($query);
?>
<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <!-- <h2>Categories</h2> -->
	</div>

	<div class="row clearfix">
    
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php
                    if(isset($_GET['insertmsg'])){
                ?>
                    <div class="alert alert-success success">
                        <?php echo $_GET['insertmsg']?>
                    </div>
                <?php } ?>

                <?php
                    if(isset($_GET['updatemsg'])){
                ?>
                    <div class="alert alert-info info">
                        <?php echo $_GET['updatemsg']?>
                    </div>
                <?php }?>
                <?php
                    if(isset($_GET['deletemsg'])){
                ?>
                    <div class="alert alert-danger danger">
                        <?php echo $_GET['deletemsg']?>
                    </div>
                <?php }?>    
                
               
                    <div class="card">
                        <div class="header">
                            <h2>
                                All Products
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Sr.#</th>
                                        <th>Title</th>
                                        <th>Category Name</th>
                                        <th>Description</th>
                                        <th>Regular Price</th>
                                        <th>Sale Price</th>
                                        <th>Picture</th>
                                        <th>Status</th>
                                        <th>Actions</th>

                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                        if($result->num_rows > 0){
                                            $i=1;
                                            while($rows=$result->fetch_assoc()){
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $rows['title']?></td>
                                            <td><?php echo $rows['category_title']?></td>
                                            <td><?php echo $rows['description']?></td>
                                            <td><?php echo $rows['regular_price']?></td>
                                            <td><?php echo $rows['sale_price']?></td>
                                            <td>
                                                <?php
                                                    $imgSrc="../../ecommerce/assets/admin/images/no-image.jpg";
                                                    if(!empty($rows['picture'])){
                                                        $imgSrc="../../ecommerce/assets/admin/images/products/".$rows['picture'];
                                                    }
                                                ?>
                                                <img src="<?php echo $imgSrc?>" alt="" width="50">
                                            </td>
                                            <td><?php echo $rows['status']?></td>
                                            <td>
                                                <a href="edit_product.php?id=<?php echo $rows['id']?>"><span class="edit-icon"><i class="fas fa-edit"></i></span></a>
                                                <a href="actions/product.php?id=<?php echo $rows['id']?>" id="deleteicon"><span class="delete-icon"><i class="fas fa-trash-alt"></i></span></a>
                                            </td>
                                        </tr>            
                                    <?php $i++;}}?>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<?php include_once('includes/footer.php'); ?>