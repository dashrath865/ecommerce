<?php 
include_once('db.php'); 
include_once('includes/header.php'); ?>
<?php include_once('includes/sidebar.php'); 

$id = $_GET['id'];
$query = "SELECT * FROM categories where id=$id";
$result = $mysqli->query($query);
$row = $result->fetch_assoc();


?>
<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <h2>Edit Category</h2>
	</div>

	<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body">
                
                        <form action="actions/category.php" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="category_id" value="<?php echo $row['id'] ?>">
                            <h2 class="card-inside-title">Basic Fields</h2>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="title" type="text" value="<?php echo $row['title'] ?>" class="form-control" placeholder="Title">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input name="slug" type="text" value="<?php echo $row['slug'] ?>" class="form-control" placeholder="slug">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix picture-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="description" type="message" class="form-control" placeholder="description"><?php echo $row['description'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line picture-box">
                                            <label>
                                               <div class="icon-and-name">
                                                    <i class="far fa-image"></i>
                                                    <span>CHOOSE FILE</span>
                                               </div>
                                                
                                                <input name="picture" type="file" class="form-control custom-file-button" >
                                            </label>
                                        </div>
                                    </div>
                                </div>   
                            </div>  

                            <div class="row clearfix status-row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <span>Status</span>
                                            <div class="radio-btn">
                                                <input type="radio" id="active" name="status" value="active" <?php if($row['status'] == 'active') echo "checked"; ?>>
                                                <label for="active">Active</label>
                                                <input type="radio" id="inactive" name="status" value="inactive" <?php if($row['status'] == 'inactive') echo "checked"; ?>>
                                                <label for="inactive">Inactive</label><br>
                                                </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-sm-6">
                                    <img src="../../ecommerce/assets/images/categories/<?php 
                                        if(empty($row['picture'])){
                                            echo $row['picture']="no-image.jpg";
                                        }
                                        else{
                                            echo $row['picture'];
                                        }
                                    ?>" alt="" width="200">
                                </div> 

                                <input name="submit" type="submit" value="Update" id="submitbtn">
                        </form>
                    </div>
                </div>

            </div>
    </div>
</section>
<?php include_once('includes/footer.php'); ?>