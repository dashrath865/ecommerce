<?php
include_once('db.php'); 
include_once('includes/header.php'); 
include_once('includes/sidebar.php'); 

//Get all categories
$query = "SELECT * FROM categories";
$result = $mysqli->query($query);
/*echo "<pre>";
print_r($result);
die;*/
?>
<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <!-- <h2>Categories</h2> -->
	</div>

	<div class="row clearfix">
    
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php 
                    if(isset($_GET['deletemsg'])){ ?>
                        <div class="alert alert-danger danger">
                            <?php echo $_GET['deletemsg']; ?>
                        </div>
                <?php }?>

                <?php
                    if(isset($_GET['insertmsg'])){?>
                        <div class="alert alert-success success">
                            <?php echo $_GET['insertmsg'];?>
                        </div>
                <?php }?>

                <?php
                    if(isset($_GET['updatemsg'])){ ?>
                        <div class="alert alert-info info">
                            <?php echo $_GET['updatemsg']?>
                        </div>
                    <?php }?>
                
                

                    <div class="card">
                        <div class="header">
                            <h2>
                                All Categories
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Picture</th>
                                        <th>Actions</th>

                                    </tr>
                                </thead>
                                <tbody class="all-categories">
                                    <?php if ($result->num_rows > 0) { 
                                        // output data of each row
                                        $i = 1;
                                        while($row = $result->fetch_assoc()) {
                                        ?>
                                    <tr>
                                        <td scope="row"><?php echo $i; ?></td>
                                        <td><?php echo $row['title']; ?></td>
                                        <td><?php echo $row['slug']; ?></td>
                                        <td><?php echo $row['description']; ?></td>
                                        <td><?php echo $row['status']; ?></td>
                                        <td>
                                        <?php $imageSrc = '../../assets\admin\images/no-image.jpg'; 
                                            if(!empty($row['picture'])){
                                                $imageSrc = "../../assets/admin/images/categories/".$row['picture']; 
                                            }
                                        ?>
                                        <img src="<?php echo $imageSrc?>" alt="" width="50" class="">
                                        </td>
                                        <td>
                                            <a href="edit_category.php?id=<?php echo $row['id']; ?>"><span class="icon edit-icon"><i class="fas fa-edit"></i></span></a>
                                        |   <a id="deleteicon" class="delete" href="actions/category.php?id=<?php echo $row['id'] ?>"><span class="icon delete-icon"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                    <?php $i++; }} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<?php include_once('includes/footer.php'); ?>