<?php 
	include_once('includes/header.php'); 
	include_once('includes/sidebar.php');
?>

<section class="content">
    <div class="container-fluid">
	<div class="block-header">
	    <h2>DASHBOARD</h2>
	</div>

	<!-- Widgets -->
	<div class="row clearfix">
	    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<?php 
            if(isset($_GET['success'])){ ?>
                <div class="alert alert-success login-success">
                	<?php echo $_GET['success']; ?>
                </div>
        <?php }?>
		<div class="info-box bg-pink hover-expand-effect">
		    <div class="icon">
			<i class="material-icons">playlist_add_check</i>
		    </div>
		    <div class="content">
			<div class="text">CATEGORIES</div>
			<div class="number count-to total-category" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
		    </div>
		</div>
	    </div>
	    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-cyan hover-expand-effect">
		    <div class="icon">
			<i class="material-icons">help</i>
		    </div>
		    <div class="content">
			<div class="text">PRODUCTS</div>
			<div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
		    </div>
		</div>
	    </div>
	    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-light-green hover-expand-effect">
		    <div class="icon">
			<i class="material-icons">forum</i>
		    </div>
		    <div class="content">
			<div class="text">ORDERS</div>
			<div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
		    </div>
		</div>
	    </div>
	    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-orange hover-expand-effect">
		    <div class="icon">
			<i class="material-icons">person_add</i>
		    </div>
		    <div class="content">
			<div class="text">CUSTOMERS</div>
			<div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
		    </div>
		</div>
	    </div>
	</div>
	<!-- #END# Widgets -->
    </div>
</section>
<?php include_once('includes/footer.php'); ?>